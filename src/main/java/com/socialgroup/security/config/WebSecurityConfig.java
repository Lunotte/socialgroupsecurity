package com.socialgroup.security.config;

import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.socialgroup.security.CustomCorsFilter;
import com.socialgroup.security.RestAuthenticationEntryPoint;
import com.socialgroup.security.auth.ajax.AjaxAuthenticationProvider;
import com.socialgroup.security.auth.ajax.AjaxLoginProcessingFilter;
import com.socialgroup.security.auth.jwt.JwtAuthenticationProvider;
import com.socialgroup.security.auth.jwt.JwtTokenAuthenticationProcessingFilter;
import com.socialgroup.security.auth.jwt.SkipPathRequestMatcher;
import com.socialgroup.security.auth.jwt.extractor.TokenExtractor;

/**
 * WebSecurityConfig
 *
 * @author vladimir.stankovic
 *
 * Aug 3, 2016
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    public static final String JWT_TOKEN_HEADER_PARAM = "X-Authorization";
    public static final String FORM_BASED_LOGIN_ENTRY_POINT = "/login";
    public static final String FORM_BASED_SWAGGER_ENTRY_POINT = "/v2/api-docs";
    public static final String TOKEN_BASED_AUTH_ENTRY_POINT = "/**";	//A Changer pour /api/** pour utiliser l'app avec auth
    public static final String TOKEN_BASED_AUTH_ENTRY_POINT_BONUS = "/manage/**";
    public static final String TOKEN_REFRESH_ENTRY_POINT = "/token";

    private final RestAuthenticationEntryPoint authenticationEntryPoint;
    private final AuthenticationSuccessHandler successHandler;
    private final AuthenticationFailureHandler failureHandler;
    private final AjaxAuthenticationProvider ajaxAuthenticationProvider;
    private final JwtAuthenticationProvider jwtAuthenticationProvider;
    private final TokenExtractor tokenExtractor;
    private final ObjectMapper objectMapper;

	public WebSecurityConfig(final RestAuthenticationEntryPoint authenticationEntryPoint,
			final AuthenticationSuccessHandler successHandler, final AuthenticationFailureHandler failureHandler,
			final AjaxAuthenticationProvider ajaxAuthenticationProvider, final JwtAuthenticationProvider jwtAuthenticationProvider,
			final TokenExtractor tokenExtractor, final ObjectMapper objectMapper) {
		super();
		this.authenticationEntryPoint = authenticationEntryPoint;
		this.successHandler = successHandler;
		this.failureHandler = failureHandler;
		this.ajaxAuthenticationProvider = ajaxAuthenticationProvider;
		this.jwtAuthenticationProvider = jwtAuthenticationProvider;
		this.tokenExtractor = tokenExtractor;
		this.objectMapper = objectMapper;
	}

	protected AjaxLoginProcessingFilter buildAjaxLoginProcessingFilter() throws Exception {
        final AjaxLoginProcessingFilter filter = new AjaxLoginProcessingFilter(FORM_BASED_LOGIN_ENTRY_POINT, successHandler, failureHandler, objectMapper);
        filter.setAuthenticationManager(authenticationManagerBean());
        return filter;
    }

    protected JwtTokenAuthenticationProcessingFilter buildJwtTokenAuthenticationProcessingFilter() throws Exception {
        final List<String> pathsToSkip = Arrays.asList(TOKEN_REFRESH_ENTRY_POINT, FORM_BASED_LOGIN_ENTRY_POINT);
        final SkipPathRequestMatcher matcher = new SkipPathRequestMatcher(pathsToSkip, TOKEN_BASED_AUTH_ENTRY_POINT);
        final JwtTokenAuthenticationProcessingFilter filter
            = new JwtTokenAuthenticationProcessingFilter(failureHandler, tokenExtractor, matcher);
        filter.setAuthenticationManager(authenticationManagerBean());
        return filter;
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(final AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(ajaxAuthenticationProvider);
        auth.authenticationProvider(jwtAuthenticationProvider);
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http
        .csrf().disable() // We don't need CSRF for JWT based authentication
        .exceptionHandling()
        .authenticationEntryPoint(this.authenticationEntryPoint)

        .and()
            .sessionManagement()
            .sessionCreationPolicy(SessionCreationPolicy.STATELESS)

        .and()
            .authorizeRequests()
                .antMatchers(FORM_BASED_LOGIN_ENTRY_POINT).permitAll() // Login end-point
                .antMatchers(FORM_BASED_SWAGGER_ENTRY_POINT).permitAll()
                .antMatchers(TOKEN_REFRESH_ENTRY_POINT).permitAll() // Token refresh end-point
                .antMatchers("/console").permitAll() // H2 Console Dash-board - only for testing
        .and()
            .authorizeRequests()
            .antMatchers(TOKEN_BASED_AUTH_ENTRY_POINT_BONUS).hasRole("MEMBER")
            .antMatchers(TOKEN_BASED_AUTH_ENTRY_POINT).authenticated() // Protected API End-points
            //.antMatchers(TOKEN_BASED_AUTH_ENTRY_POINT).hasRole("MEMBER")
           // .antMatchers(TOKEN_BASED_AUTH_ENTRY_POINT_BONUS).hasRole("MEMBER")
           // .antMatchers(TOKEN_BASED_AUTH_ENTRY_POINT).hasAnyRole("MEMBER_BONUS","MEMBER")

        .and()
            .addFilterBefore(new CustomCorsFilter(), UsernamePasswordAuthenticationFilter.class)
            .addFilterBefore(buildAjaxLoginProcessingFilter(), UsernamePasswordAuthenticationFilter.class)
            .addFilterBefore(buildJwtTokenAuthenticationProcessingFilter(), UsernamePasswordAuthenticationFilter.class);
    }
}
