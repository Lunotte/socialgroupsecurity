package com.socialgroup.security.auth.ajax;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.socialgroup.security.model.User;
import com.socialgroup.security.model.UserContext;
import com.socialgroup.security.model.UserGroup;
import com.socialgroup.security.service.UserService;

/**
 *
 * @author vladimir.stankovic
 *
 * Aug 3, 2016
 */
@Component
public class AjaxAuthenticationProvider implements AuthenticationProvider {
    private final BCryptPasswordEncoder encoder;
    private final UserService userService;

    public AjaxAuthenticationProvider(final UserService userService, final BCryptPasswordEncoder encoder) {
        this.userService = userService;
        this.encoder = encoder;
    }

    @Override
    public Authentication authenticate(final Authentication authentication) throws AuthenticationException {
        Assert.notNull(authentication, "No authentication data provided");

        final String email = (String) authentication.getPrincipal();
        final String password = (String) authentication.getCredentials();

        final User user = userService.getByEmailAuth(email).orElseThrow(() -> new UsernameNotFoundException("User not found: " + email));

        if (!encoder.matches(password, user.getPassword())) {
            throw new BadCredentialsException("Authentication Failed. Username or Password not valid.");
        }

        if (user.getRoles() == null) {
			throw new InsufficientAuthenticationException("User has no roles assigned");
		}

        // On met à jour la dernière date de connexion
        final Optional<UserGroup> userGroup = user.getUserGroups().stream()
        								.filter(ug -> ug.getGroup().equals(user.getGroup()) && ug.getUser().equals(user))
        								.findFirst();
        if(userGroup.isPresent()) {
        	userGroup.get().setLastConnextion(ZonedDateTime.now());
        	this.userService.save(user);
        }

        final List<GrantedAuthority> authorities = user.getRoles().stream()
                .map(authority -> new SimpleGrantedAuthority(authority.getRole().authority()))
                .collect(Collectors.toList());

        final UserContext userContext = UserContext.create(user.getId().toString(), authorities);

        return new UsernamePasswordAuthenticationToken(userContext, null, userContext.getAuthorities());
    }

    @Override
    public boolean supports(final Class<?> authentication) {
        return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
    }
}
