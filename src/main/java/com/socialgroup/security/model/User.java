package com.socialgroup.security.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

@Entity
@Table(name = "sg_users")
public class User implements Serializable{

	private static final long serialVersionUID = -4755320225073186656L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(updatable = false, nullable = false)
	private Long id;

	@Column(name = "email", nullable=false, unique=true)
	private String email;

	@Column(name = "born")
	private LocalDate born;

	@Column(name = "avatar")
	private String avatar;

	@Column(name = "firstname", nullable=false)
	private String firstname;

	@Column(name = "lastname", nullable=false)
	private String lastname;

	@Column(name = "username", nullable=false)
	private String username;

	@Column(name = "password", nullable=false)
	private String password;

	@OrderBy("lastConnextion DESC")
	@OneToMany(mappedBy = "user", fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	private Set<UserGroup> userGroups = new HashSet<>();

	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name = "APP_USER_ID", referencedColumnName = "ID")
	private Set<UserRole> roles = new HashSet<>();

	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "group_default_id", nullable = false)
	private Group group;

	public String getEmail() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(final String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(final String lastname) {
		this.lastname = lastname;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(final String avatar) {
		this.avatar = avatar;
	}

	public User() {
		super();
	}

	public Long getId() {
		return id;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public Set<UserRole> getRoles() {
		return roles;
	}

	public Set<UserGroup> getUserGroups() {
		return userGroups;
	}

	public void setUserGroups(final Set<UserGroup> userGroups) {
		this.userGroups = userGroups;
	}

	public void addUserGroups(final Collection<UserGroup> userGroups) {
		for (final UserGroup userGroup : userGroups) {
			addUserGroup(userGroup);
		}
	}

	public void addUserGroup(final UserGroup userGroup) {
		if(!this.userGroups.contains(userGroup)) {
			this.userGroups.add(userGroup);
		}
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public void setUsername(final String username) {
		this.username = username;
	}

	public void setPassword(final String password) {
		this.password = password;
	}

	public void setRoles(final Set<UserRole> roles) {
		this.roles = roles;
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(final Group group) {
		this.group = group;
	}

	public LocalDate getBorn() {
		return born;
	}

	public void setBorn(final LocalDate born) {
		this.born = born;
	}

	public void addUserRole(final UserRole userRole) {
		if(!this.roles.contains(userRole)) {
			this.roles.add(userRole);
		}
	}

}
