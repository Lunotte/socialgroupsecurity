package com.socialgroup.security.model;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;

/**
 *
 * @author vladimir.stankovic
 *
 * Aug 4, 2016
 */
public class UserContext {
    private final String unique;
    private final List<GrantedAuthority> authorities;

    private UserContext(String unique, List<GrantedAuthority> authorities) {
        this.unique = unique;
        this.authorities = authorities;
    }

    public static UserContext create(String username, List<GrantedAuthority> authorities) {
        if (StringUtils.isBlank(username))
		{
			throw new IllegalArgumentException("Username is blank: " + username);
		}
        return new UserContext(username, authorities);
    }

    public String getUnique() {
        return unique;
    }

    public List<GrantedAuthority> getAuthorities() {
        return authorities;
    }
}
