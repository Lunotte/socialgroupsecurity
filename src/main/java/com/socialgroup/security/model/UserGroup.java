package com.socialgroup.security.model;

import java.time.ZonedDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Entity
@Table(name = "sg_user_groups")
public class UserGroup {

	@EmbeddedId
	private UserGroupKey id;

	@ManyToOne(cascade = CascadeType.PERSIST)
    @MapsId("user_id")
    @JoinColumn(name = "user_id")
	private User user;

	@ManyToOne(cascade = CascadeType.PERSIST)
    @MapsId("group_id")
    @JoinColumn(name = "group_id")
	private Group group;

	@Column(name = "connected")
	private ZonedDateTime lastConnextion;

	public UserGroup() {
		super();
	}

	public UserGroup(final User user, final Group group) {
		this.user = user;
		this.group = group;
		this.lastConnextion = ZonedDateTime.now();
		this.id = new UserGroupKey(user.getId(), group.getId());
	}

	public UserGroupKey getId() {
		return id;
	}

	public void setId(final UserGroupKey id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(final User user) {
		this.user = user;
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(final Group group) {
		this.group = group;
	}

	public ZonedDateTime getLastConnextion() {
		return lastConnextion;
	}

	public void setLastConnextion(final ZonedDateTime lastConnextion) {
		this.lastConnextion = lastConnextion;
	}

}

