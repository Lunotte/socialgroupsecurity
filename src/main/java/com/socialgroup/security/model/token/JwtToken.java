package com.socialgroup.security.model.token;

public interface JwtToken {
    String getToken();
}
