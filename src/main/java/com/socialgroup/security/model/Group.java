package com.socialgroup.security.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "sg_groups")
public class Group implements Serializable{

	private static final long serialVersionUID = -932631652204586962L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "picture")
	private String picture;

	@OneToMany(mappedBy = "group", cascade = CascadeType.ALL)
	private Set<UserGroup> userGroups = new HashSet<>();

	public Group() {
	}

	public Long getId() {
		return id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public Set<UserGroup> getUserGroups() {
		return userGroups;
	}

	public void setUsers(final Set<UserGroup> userGroups) {
		this.userGroups = userGroups;
	}

	public void addUserGroup(final UserGroup userGroup) {
		if(!this.userGroups.contains(userGroup)) {
			this.userGroups.add(userGroup);
		}
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(final String picture) {
		this.picture = picture;
	}

}
