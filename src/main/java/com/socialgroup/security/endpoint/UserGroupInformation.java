package com.socialgroup.security.endpoint;

public class UserGroupInformation {

	private final Long user;
	private final Long group;

	public UserGroupInformation(final Long user, final Long group)
	{
		this.user = user;
		this.group = group;
	}

	public Long getUser()
	{
		return user;
	}

	public Long getGroup()
	{
		return group;
	}

}
