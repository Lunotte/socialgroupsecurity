package com.socialgroup.security.endpoint;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.socialgroup.common.exception.SgInvalidArgumentException;
import com.socialgroup.security.auth.JwtAuthenticationToken;
import com.socialgroup.security.model.User;
import com.socialgroup.security.model.UserContext;
import com.socialgroup.security.service.UserService;

@RestController
public class AuthController {

	private static final Logger LOGGER = LoggerFactory.getLogger(AuthController.class);

	private final UserService userService;

	public AuthController(final UserService userService)
	{
		this.userService = userService;
	}

	@GetMapping(value = "/me")
	public @ResponseBody String me(final JwtAuthenticationToken token) {
		return ((UserContext) token.getPrincipal()).getUnique();
	}

	@GetMapping(value = "/authenticated")
	public @ResponseBody UserContext context(final JwtAuthenticationToken token) {
		return (UserContext) token.getPrincipal();
	}

	@GetMapping(value = "/error")
	public @ResponseBody ResponseEntity<String> error(final JwtAuthenticationToken token) {
		return new ResponseEntity<String>("No message available", HttpStatus.NOT_FOUND);
	}

	@GetMapping(value = "/user-exist")
	public @ResponseBody ResponseEntity<Boolean> userExist(final JwtAuthenticationToken token) {
		return new ResponseEntity<Boolean>(token.isAuthenticated(), HttpStatus.OK);
	}

	@GetMapping(value = "/user-group-information")
	public @ResponseBody UserGroupInformation userGroupInformation(final JwtAuthenticationToken token) throws NumberFormatException, Exception {
		LOGGER.debug("Get user and group connected");
		String userId = ((UserContext) token.getPrincipal()).getUnique();
		Optional<User> user = userService.findById(Long.valueOf(userId));
		if(!user.isPresent()) {
			throw new SgInvalidArgumentException("User has not found");
		}
		User userTmp = user.get();
		if(userTmp.getGroup() == null) {
			// throw new SgInvalidArgumentException("An error was occurred to find the active group");
			throw new SgInvalidArgumentException("An error was occurred to find the active group");
		}
		return new UserGroupInformation(userTmp.getId(), userTmp.getGroup().getId());
	}
}
