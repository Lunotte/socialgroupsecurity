package com.socialgroup.security.endpoint;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.socialgroup.security.auth.jwt.extractor.TokenExtractor;
import com.socialgroup.security.auth.jwt.verifier.TokenVerifier;
import com.socialgroup.security.config.JwtSettings;
import com.socialgroup.security.config.WebSecurityConfig;
import com.socialgroup.security.exceptions.InvalidJwtToken;
import com.socialgroup.security.model.User;
import com.socialgroup.security.model.UserContext;
import com.socialgroup.security.model.token.JwtToken;
import com.socialgroup.security.model.token.JwtTokenFactory;
import com.socialgroup.security.model.token.RawAccessJwtToken;
import com.socialgroup.security.model.token.RefreshToken;
import com.socialgroup.security.service.UserService;

/**
 * RefreshTokenEndpoint
 *
 * @author vladimir.stankovic
 *
 * Aug 17, 2016
 */
@RestController
public class RefreshTokenEndpoint {

    private final JwtTokenFactory tokenFactory;
    private final JwtSettings jwtSettings;
    private final UserService userService;
    private final TokenVerifier tokenVerifier;
    @Qualifier("jwtHeaderTokenExtractor") private final TokenExtractor tokenExtractor;

    public RefreshTokenEndpoint(final JwtTokenFactory tokenFactory, final JwtSettings jwtSettings, final UserService userService,
			final TokenVerifier tokenVerifier, final TokenExtractor tokenExtractor) {
		super();
		this.tokenFactory = tokenFactory;
		this.jwtSettings = jwtSettings;
		this.userService = userService;
		this.tokenVerifier = tokenVerifier;
		this.tokenExtractor = tokenExtractor;
	}

	@RequestMapping(value="/api/auth/token", method=RequestMethod.GET, produces={ MediaType.APPLICATION_JSON_VALUE })
    public @ResponseBody JwtToken refreshToken(final HttpServletRequest request, final HttpServletResponse response) throws NumberFormatException, Exception {
        final String tokenPayload = tokenExtractor.extract(request.getHeader(WebSecurityConfig.JWT_TOKEN_HEADER_PARAM));

        final RawAccessJwtToken rawToken = new RawAccessJwtToken(tokenPayload);
        final RefreshToken refreshToken = RefreshToken.create(rawToken, jwtSettings.getTokenSigningKey()).orElseThrow(() -> new InvalidJwtToken());

        final String jti = refreshToken.getJti();
        if (!tokenVerifier.verify(jti)) {
            throw new InvalidJwtToken();
        }

        final String subject = refreshToken.getSubject();
        final User user = userService.findById(Long.valueOf(subject)).orElseThrow(() -> new UsernameNotFoundException("User not found: " + subject));

        if (user.getRoles() == null) {
			throw new InsufficientAuthenticationException("User has no roles assigned");
		}
        final List<GrantedAuthority> authorities = user.getRoles().stream()
                .map(authority -> new SimpleGrantedAuthority(authority.getRole().authority()))
                .collect(Collectors.toList());

        final UserContext userContext = UserContext.create(user.getUsername(), authorities);

        return tokenFactory.createAccessJwtToken(userContext);
    }
}
