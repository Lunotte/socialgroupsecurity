package com.socialgroup.security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableDiscoveryClient
@EnableSwagger2
@EnableConfigurationProperties
@EnableAutoConfiguration
public class SocialGroupSecurityApplication {

	public static void main(final String[] args) {
		SpringApplication.run(SocialGroupSecurityApplication.class, args);
	}

}
