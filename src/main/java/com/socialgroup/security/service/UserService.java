package com.socialgroup.security.service;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.socialgroup.security.model.User;

/**
 * Mock implementation.
 *
 * @author vladimir.stankovic
 *
 * Aug 4, 2016
 */
@Service
public class UserService {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    private final UserRepository userRepository;

    public UserService(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

	public Optional<User> getByEmailAuth(final String email) {

		LOGGER.debug("Find the user {}", email);
		return this.userRepository.findByEmailAuth(email);
    }

	public Optional<User> findById(final Long userId) throws Exception {
		Assert.notNull(userId, "You have to define the user");
		LOGGER.debug("Find the user {}", userId);

		try {
			return this.userRepository.findById(userId);
		} catch (final Exception e) {
    		throw new Exception(String.format("An error has occurred to find the user %d", userId), e);
    	}
    }

	public User save(final User user) {
		return this.userRepository.save(user);
	}
}
