package com.socialgroup.security.service;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.socialgroup.security.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	public Optional<User> getByUsername(String username);

    @Query("select u from User u left join fetch u.roles r where u.email=:email")
    public Optional<User> findByEmailAuth(@Param("email") String email);

    public Optional<User> findByEmail(String email);

}
